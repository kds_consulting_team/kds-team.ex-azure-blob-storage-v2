1. Account Name - `Required`
    - Azure Storage Account Name
2. Account Key - `Required`
    - Access keys to authentication when making requests to the Azure Storage Account
    - Access Keys can be found under the **Settings** of the Storage Account
    - **Settings** >> **Access keys** >> **Key** from key1 or key2
3. Container Name - `Required`
    - Azure Storage Container Name