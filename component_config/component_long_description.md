This component is to extract a single or multiple files from a single or nested folders from an Azure Blob Container and stores them in Storage.

By default, the component expects CSV files that are stored in [table storage](https://help.keboola.com/storage/tables/), but it can handle any file type or destination using [processors](https://components.keboola.com/components?type=processor).

## Configurations

1. Account Name - `Required`
    - Azure Storage Account Name
2. Account Key - `Required`
    - Access keys to authentication when making requests to the Azure Storage Account
    - Access Keys can be found under the **Settings** of the Storage Account
    - **Settings** >> **Access keys** >> **Key** from key1 or key2
3. Container Name - `Required`
    - Azure Storage Container Name