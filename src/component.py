'''
Template Component main class.

'''

import datetime
import logging
import os
import sys
from typing import Dict

import logging_gelf.handlers  # noqa
from azure.core.exceptions import ResourceNotFoundError, HttpResponseError
# from azure.storage.blob import BlockBlobService, PublicAccess  # noqa
from azure.identity import AuthorizationCodeCredential  # noqa
from azure.storage.blob import ContainerClient
from azure.storage.blob._shared.authentication import AzureSigningError
from kbc.env_handler import KBCEnvHandler
from kbc.result import KBCTableDef  # noqa
from kbc.result import ResultWriter  # noqa
from kbcstorage.workspaces import Workspaces
from msal import token_cache  # noqa
from requests import HTTPError

from blob_procedure import Blob_Procedure, BlobRequestError

# configuration variables
KEY_AUTH_TYPE = "auth_type"

KEY_ACCOUNT_NAME = 'account_name'
KEY_ACCOUNT_KEY = '#account_key'

KEY_WORKSPACE_ID = "workspace_id"
KEY_STORAGE_TOKEN = "#storage_token"

KEY_CONTAINER_NAME = 'container_name'
KEY_NEW_FILE_ONLY = 'new_files_only'
KEY_FILE = 'file'
KEY_DEBUG = 'debug'
KEY_BLOB_DOMAIN = 'blob_domain'

WORKSPACE_AUTH_TYPE = "Workspace Credentials"
AZURE_AUTH_TYPE = "Azure Credentials"

MANDATORY_PARS = [
    KEY_ACCOUNT_NAME,
    KEY_ACCOUNT_KEY,
    KEY_CONTAINER_NAME,
    KEY_FILE
]
MANDATORY_IMAGE_PARS = []

DEFAULT_BLOB_DOMAIN = 'blob.core.windows.net'

APP_VERSION = '0.2.1'


class UserException(Exception):
    pass


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        # Disabling list of libraries you want to output in the logger
        disable_libraries = [
            'azure.core.pipeline.policies.http_logging_policy'
        ]
        for library in disable_libraries:
            logging.getLogger(library).disabled = True

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    @staticmethod
    def validate_config_params(params: Dict) -> None:
        """
        Validating if input configuration contain everything needed
        """

        # Credentials Conditions
        # Validate if config is blank
        if params == {}:
            raise UserException('Configurations are missing. Please configure your component.')

        # Validate if the configuration is empty
        empty_config = {
            'account_name': '',
            '#account_key': '',
            'container_name': ''
        }
        if params == empty_config:
            raise UserException('Configurations are missing. Please configure your component.')

        # Validating config parameters
        if not params.get(KEY_ACCOUNT_NAME):
            raise UserException("Credientials missing: Account Name")

        if params.get(KEY_AUTH_TYPE, AZURE_AUTH_TYPE) == AZURE_AUTH_TYPE and not params.get(KEY_ACCOUNT_KEY):
            raise UserException("Credientials missing: Access Key.")

        if not params.get(KEY_CONTAINER_NAME):
            raise UserException("Blob Container name is missing, check your configuration.")

    def run(self):
        """
        Main execution code
        """

        params = self.cfg_params  # noqa
        self.validate_config_params(params)

        # Get proper list of tables
        now = int(datetime.datetime.utcnow().timestamp())
        account_name = params.get(KEY_ACCOUNT_NAME)
        account_key = params.get(KEY_ACCOUNT_KEY)  # noqa
        container_name = params.get(KEY_CONTAINER_NAME)
        file = params.get(KEY_FILE)

        # Validating user input file configuration
        if not file:
            logging.error('File configuration is missing.')
            sys.exit(1)

        # New file parameter if file configuration exists
        new_file_only = file.get(KEY_NEW_FILE_ONLY)
        logging.info(f'New File Only: {new_file_only}')
        blob_domain = params.get(KEY_BLOB_DOMAIN, DEFAULT_BLOB_DOMAIN)
        account_url = f'{account_name}.{blob_domain}'

        # Get state file
        state = self.get_state_file()  # noqa
        if state and new_file_only:
            last_run_timestamp = state.get('lastRunTimestamp')
            logging.info(f'Extracting from: {last_run_timestamp}')

        else:
            last_run_timestamp = None
            state = {}

        if params.get(KEY_AUTH_TYPE, AZURE_AUTH_TYPE) == WORKSPACE_AUTH_TYPE:
            workspace_token = params.get(KEY_STORAGE_TOKEN)
            workspace_id = params.get(KEY_WORKSPACE_ID)
            workspace_client = Workspaces(f'https://{os.environ.get("KBC_STACKID")}', workspace_token)
            account_key = self._refresh_abs_container_token(workspace_client, workspace_id)

        # Initializing Blob Container client
        blob_container_client = ContainerClient(
            account_url=account_url,
            container_name=container_name,
            credential=account_key
        )

        # Validating Credentials
        # Exceptions Handling
        try:
            logging.info("Validating connection.")
            blob_container_client.get_account_information()

        except AzureSigningError:
            # Credentials and Account Name validation
            logging.error(
                'The specified credentials [Account Name] & [Account Key]/[SAS token] are invalid.')
            sys.exit(1)

        except ResourceNotFoundError as e:
            # Container validation
            logging.error(e)
            sys.exit(1)
        except HttpResponseError as e:
            logging.error(e)
            sys.exit(1)

        except Exception as e:
            # If there are any other errors, reach out support
            logging.error(e)
            logging.error('Please validate your [Account Name].')
            sys.exit(1)

        # Processing blobs
        Blob_Procedure(
            blob_obj=blob_container_client,
            file=file,
            current_timestamp=now,
            DEFAULT_TABLE_SOURCE=f'{self.tables_in_path}/',
            DEFAULT_TABLE_DESTINATION=f'{self.files_out_path}/',
            last_run_timestamp=last_run_timestamp
        )

        # Updating State file regardless
        state = {'lastRunTimestamp': now}
        self.write_state_file(state)
        logging.debug(f'Writing State: {state}')

        logging.info("Blob Storage Extraction finished")

    @staticmethod
    def _refresh_abs_container_token(workspace_client: Workspaces, workspace_id: str) -> str:
        try:
            ps = workspace_client.reset_password(workspace_id)
            return ps['connectionString'].split('SharedAccessSignature=')[1]
        except HTTPError as e:
            if e.response.status_code == 404:
                raise UserException(f"Workspace ID: {workspace_id} was not found in the destination project. "
                                    f"Please check the ID and verify the correct Storage token is entered.") from e
            elif e.response.status_code == 401:
                raise UserException("Failed to refresh the SAS token. Invalid Storage token.") from e
            elif 400 >= e.response.status_code < 500:
                raise UserException(e) from e
            elif 500 >= e.response.status_code:
                raise e


"""
        Main entrypoint
"""

if __name__ == "__main__":
    debug = sys.argv[1] if len(sys.argv) > 1 else True

    try:
        comp = Component(debug)
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except BlobRequestError as e:
        logging.error(e)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
