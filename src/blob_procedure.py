import datetime
import logging
import sys
import time
from pathlib import Path

import backoff
from azure.core.exceptions import ResourceModifiedError, HttpResponseError
from azure.storage.blob import ContainerClient
from azure.storage.blob._models import BlobPrefix
from fnmatch2 import fnmatch2


def _retry_on_race_condititon(details):
    logging.warning(f"Resource modified during extraction, retrying in {details['wait']}")


class BlobRequestError(Exception):
    pass


class Blob_Procedure():

    def __init__(self, blob_obj,
                 file,
                 current_timestamp,
                 DEFAULT_TABLE_SOURCE="/data/in/tables/",
                 DEFAULT_TABLE_DESTINATION="/data/out/files/",
                 last_run_timestamp=None):
        self.blob_obj: ContainerClient = blob_obj
        self.last_run_timestamp = last_run_timestamp
        self.current_timestamp = current_timestamp
        self.DEFAULT_TABLE_SOURCE = DEFAULT_TABLE_SOURCE
        self.DEFAULT_TABLE_DESTINATION = DEFAULT_TABLE_DESTINATION
        try:
            self.standard_process(file)
        except HttpResponseError as e:
            if e.status_code == 403:
                error = f'The credentials do not have permission to access the file {file}. ' \
                        'Check if the token has read & list permissions.'
            elif e.status_code == 401:
                error = 'The credentials are invalid! Check the SAS key / token'
            else:
                error = e.reason
            raise BlobRequestError(error)

    def list_all_blobs_legacy(self):
        return self.blob_obj.list_blobs(retry_total=0)

    def standard_process(self, file: dict):
        """
        Main process to download the blob file(s)
        :param file:
        :return:
        """

        logging.info("Listing available blobs")
        try:
            # fallback to legacy listing method for backward compatibility
            if "**" in file["file_name"]:
                all_files = self.list_all_blobs_legacy()
            else:
                all_files = self.list_blobs_hierarchical_listing(file)
        except Exception as err:
            # TODO: raise exception instead of exit
            logging.exception(f'Error occurred while listing blob files: {err}')
            logging.error('Please contact support.')
            sys.exit(1)

        logging.info('Processing [{}]...'.format(file['file_name']))
        start = time.time()

        blobs_to_download = self.qualify_files(
            file_pattern=file['file_name'], blob_files=all_files)

        end = time.time()
        logging.debug(f'List blobs: {end - start}')

        # To add timestamp into the file or not
        add_timestamp_bool = file['add_timestamp'] if 'add_timestamp' in file else False

        # When there is nothing to download
        if len(blobs_to_download) == 0:
            return

        start = time.time()
        for _index, blob in enumerate(blobs_to_download):

            blob_full_filename = blob['name']
            blob_created_timestamp = blob['created_timestamp']
            logging.info(f'Downloading [{blob_full_filename}]...')

            # Blob Paths and filenames
            # blob_file_full_path needs to be relative path, otherwise the join will fail
            if blob_full_filename.startswith('/'):
                blob_full_filename = blob_full_filename.split(
                    '/', maxsplit=1)[1]

            output_dir_path = Path(self.DEFAULT_TABLE_DESTINATION)
            # blob_file_full_path = Path(blob_full_filename)
            blob_file_full_path = blob_full_filename.replace('/', '_')

            output_full_file_path = output_dir_path.joinpath(
                blob_file_full_path)

            # append unique id to file, because there can be blob and folder of a same name in one folder
            output_full_file_path = output_full_file_path.parent.joinpath(
                f'{_index}_{output_full_file_path.name}')

            # Append timestamp
            if add_timestamp_bool:
                output_full_file_path = output_full_file_path.parent.joinpath(
                    f'{blob_created_timestamp}_{output_full_file_path.name}')

            # Creating directory
            output_full_file_path.parent.mkdir(parents=True, exist_ok=True)

            logging.debug(f'{blob_file_full_path} --> {output_full_file_path}')

            # download with retry rather than creating a lease
            self._download_into_file(output_full_file_path, blob_full_filename)

        end = time.time()
        logging.debug(f'Download blobs: {end - start}')

    def get_file_folders(self, file_name):
        file_paths = self.get_file_path(file_name)
        if len(file_paths) > 1:
            return file_paths[:-1]
        return []

    @staticmethod
    def get_file_path(file_name):
        return file_name.split("/")

    @staticmethod
    def get_file_name_from_path(file_name):
        file_paths = file_name.split("/")
        if len(file_paths) > 1:
            return file_paths[-1]
        return file_name

    def list_blobs_hierarchical_listing(self, file: dict):
        """
        List blobs by hierarchically walking the directories and filtering on path prefix.
        :param file: (dict) file defintion / pattern
        :return:
        """
        file_folders = self.get_file_folders(file['file_name'])
        longest_direct_path = self.get_longest_direct_path(file_folders)

        files = []

        def walk_blob_hierarchy(prefix=""):
            for item in self.blob_obj.walk_blobs(name_starts_with=prefix):
                if isinstance(item, BlobPrefix):
                    if fnmatch2(item.name, file["file_name"]):
                        walk_blob_hierarchy(prefix=item.name)
                    elif self.matches(item, file_folders):
                        walk_blob_hierarchy(prefix=item.name)
                    else:
                        logging.debug(f"ignoring folder {item.name}")
                        continue
                else:
                    if fnmatch2(item.name, file['file_name']):
                        files.append(item)

        walk_blob_hierarchy(prefix=longest_direct_path)
        return files

    @staticmethod
    def get_item_folders(item):
        item_folders = item.name.split("/")
        if not item_folders[-1]:
            item_folders = item_folders[0:-1]
        return item_folders

    @staticmethod
    def is_direct_path(path):
        if any(character in path for character in ["*", "[", "]", "?", "!"]):
            return False
        return True

    def get_longest_direct_path(self, path_list):
        longest_direct_path = []
        for path in path_list:
            if self.is_direct_path(path):
                longest_direct_path.append(path)
            else:
                break
        return "/".join(longest_direct_path)

    def matches(self, item, file_folders):
        item_folders = self.get_item_folders(item)
        if len(file_folders) < len(item_folders):
            return False
        for i, folder in enumerate(item_folders):
            if not fnmatch2(item_folders[i], file_folders[i]):
                return False
        return True

    @backoff.on_exception(backoff.expo,
                          ResourceModifiedError,
                          max_tries=3,
                          on_backoff=_retry_on_race_condititon
                          )
    def _download_into_file(self, output_full_file_path, blob_full_filename):
        with open(output_full_file_path, 'wb') as my_blob:
            blob_data = self.blob_obj.download_blob(
                blob=blob_full_filename)
            blob_data.readinto(my_blob)

    def qualify_files(self, file_pattern, blob_files=None):
        '''
        Fetching BLOBs matches with GLOB configuration
        '''
        qualified_files = []
        qualified_files_short = []

        for blob in blob_files:

            blob_name = blob['name']
            blob_last_modified = datetime.datetime.timestamp(
                blob['last_modified'])
            blob_creation_date = datetime.datetime.timestamp(
                blob['creation_time'])

            match_bool = fnmatch2(blob_name, file_pattern)
            logging.debug(
                f'Matched: {match_bool} | {file_pattern} <> {blob_name}')

            if match_bool is True:
                blob_obj = {}
                if self.last_run_timestamp:
                    if int(blob_last_modified) >= int(self.last_run_timestamp):
                        logging.debug('New File: True')
                        blob_obj['name'] = blob_name
                        blob_obj['created_timestamp'] = blob_creation_date
                        qualified_files.append(blob_obj)
                        qualified_files_short.append(blob_name)

                    else:
                        logging.debug('New File: False')

                else:
                    # qualified_files += [blob_name]
                    blob_obj['name'] = blob_name
                    blob_obj['created_timestamp'] = int(blob_creation_date)
                    qualified_files.append(blob_obj)
                    qualified_files_short.append(blob_name)

        logging.info(f'Number of qualified blob files: {len(qualified_files)}')
        logging.debug(f'Qualified Files: {qualified_files_short}')

        return qualified_files
