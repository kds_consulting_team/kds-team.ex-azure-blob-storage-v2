# Azure Blob Storage Extractor

This component is to extract a single or multiple CSV files from a single or nested folders from a Azure Blob Container and stores them into Keboola.

## Configurations

- **Account Name** - `Required`
    - Azure Storage Account Name
- **Account Key (Or SAS Token)** - `Required`
    - Account Key can be found:
      ```
      [Your Storage Account Overview] > Settings > Access Keys
      ```
    - SAS token can be generated from the Azure portal. More info [here](https://docs.microsoft.com/en-us/azure/storage/common/storage-sas-overview):
      ```
      [Your Storage Account Overview] > Your Blob Container > Shared access tokens
      ```   
- **Container Name** - `Required`
    - Azure Storage Container Name
    
    
- **File name** - Name of a source file with its extension or wildcard.
`folder/subfolder/test.csv` will download "test" CSV file from "folder/subfolder" directory
`test_*` will download all CSV files with "test_" prefix
- **New files only** - Every job stores the timestamp of the last downloaded file and a subsequent job can pick up from there.


### Raw JSON configuration example

```json
{ "parameters":{
        "account_name": "ACCOUNTNAME",
        "#account_key": "XXXXX",
        "path": "folder/subfolder/test.csv",
        "container_name": "keboola-test",
        "file":
            {
                "file_name": "testing/*",
                "storage": "testing",
                "incremental": false,
                "primary_key": []
            },
        "debug": true
    }
}
```